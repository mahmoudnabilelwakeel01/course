<?php

use App\Http\Controllers\BookController;
use App\Http\Controllers\CourseBookController;
use App\Http\Controllers\CourseController;
use App\Http\Controllers\CourseVideoController;
use App\Http\Controllers\SubscriberController;
use App\Http\Controllers\SubscriberCourseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('book')->name('book.')->group(function () {
    Route::get('/show', [BookController::class, 'show'])->name('show');
    Route::post('/store', [BookController::class, 'store'])->name('store');
    Route::get('/find/{id}', [BookController::class, 'find'])->name('find');
    Route::post('/update/{id}', [BookController::class, 'update'])->name('update');
    Route::get('/delete/{id}', [BookController::class, 'delete'])->name('delete');
});

Route::prefix('course')->name('course.')->group(function () {
    Route::get('/show', [CourseController::class, 'show'])->name('show');
    Route::post('/store', [CourseController::class, 'store'])->name('store');
    Route::get('/find/{id}', [CourseController::class, 'find'])->name('find');
    Route::post('/update/{id}', [CourseController::class, 'update'])->name('update');
    Route::get('/delete/{id}', [CourseController::class, 'delete'])->name('delete');

    Route::prefix('video')->name('video.')->group(function () {
        Route::get('/show', [CourseVideoController::class, 'show'])->name('show');
        Route::post('/store', [CourseVideoController::class, 'store'])->name('store');
        Route::get('/find/{id}', [CourseVideoController::class, 'find'])->name('find');
        Route::post('/update/{id}', [CourseVideoController::class, 'update'])->name('update');
        Route::get('/delete/{id}', [CourseVideoController::class, 'delete'])->name('delete');
    });

    Route::prefix('book')->name('book.')->group(function () {
        Route::get('/show', [CourseBookController::class, 'show'])->name('show');
        Route::post('/store', [CourseBookController::class, 'store'])->name('store');
        Route::get('/find/{id}', [CourseBookController::class, 'find'])->name('find');
        Route::post('/update/{id}', [CourseBookController::class, 'update'])->name('update');
        Route::get('/delete/{id}', [CourseBookController::class, 'delete'])->name('delete');
    });
});

Route::prefix('subscriber')->name('subscriber.')->group(function () {
    Route::get('/show', [SubscriberController::class, 'show'])->name('show');
    Route::post('/store', [SubscriberController::class, 'store'])->name('store');
    Route::get('/find/{id}', [SubscriberController::class, 'find'])->name('find');
    Route::post('/update/{id}', [SubscriberController::class, 'update'])->name('update');
    Route::get('/delete/{id}', [SubscriberController::class, 'delete'])->name('delete');

    Route::prefix('course')->name('course.')->group(function () {
        Route::get('/show', [SubscriberCourseController::class, 'show'])->name('show');
        Route::post('/store', [SubscriberCourseController::class, 'store'])->name('store');
        Route::get('/find/{id}', [SubscriberCourseController::class, 'find'])->name('find');
        Route::post('/update/{id}', [SubscriberCourseController::class, 'update'])->name('update');
        Route::get('/delete/{id}', [SubscriberCourseController::class, 'delete'])->name('delete');
    });
});

