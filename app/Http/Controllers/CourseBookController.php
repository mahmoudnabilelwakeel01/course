<?php

namespace App\Http\Controllers;

use App\Models\CourseBook;
use Illuminate\Http\Request;

class CourseBookController extends Controller
{
    public function show()
    {
        $courseBooks = CourseBook::get();
        return response()->json($courseBooks);
    }

    public function store(Request $request)
    {
        try {
            if ($request->course_id == '' || $request->book_id == '' ) {
                return response()->json(['message' => "من فضلك اكمل البيانات"]);
            }

            $courseBook = CourseBook::create([
                'course_id'=> $request->course_id,
                'book_id'=> $request->book_id,
            ]);
            return response()->json(['message' => "تم إضافة الكتاب للكورس بنجاح"]);
        } catch (\Exception $e) {
            return response()->json(['message' => "خطأ اثناء إضافة الكتاب للكورس"]);
        }
    }

    public function find($id)
    {
        $courseBook = CourseBook::findOrFail($id);
        return response()->json($courseBook);
    }

    public function update(Request $request, $id)
    {
        try {
            $request->validate([
                'course_id' => 'required',
                'book_id' => 'required',
            ]);
            $courseBook = CourseBook::findOrFail($id);
            $courseBook->course_id = $request->course_id;
            $courseBook->book_id = $request->book_id;

            $courseBook->update();
            return response()->json(['message' => "تم تعديل الكتاب بنجاح"]);
        } catch (\Exception $e) {
            return response()->json(['message' => "خطأ اثناء تعديل الكتاب"]);
        }
    }

    public function delete($id)
    {
        try {
            $courseBook = CourseBook::findOrFail($id);
            $courseBook->delete();
            return response()->json(['message' => "تم حذف الكتاب بنجاح"]);
        } catch (\Exception $e) {
            return response()->json(['message' => "خطأ اثناء حذف الكتاب"]);
        }
    }
}
