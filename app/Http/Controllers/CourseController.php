<?php

namespace App\Http\Controllers;

use App\Models\Course;
use Illuminate\Http\Request;

class CourseController extends Controller
{
    public function show()
    {
        $courses = Course::get();
        return response()->json($courses);
    }

    public function store(Request $request)
    {
        try {
            if ($request->name == '' || $request->photo == '') {
                return response()->json(['message' => "من فضلك اكمل البيانات"]);
            }

            $photo = $request->photo->store('/public/course');

            $course = Course::create([
                'name' => $request->name,
                'price' => $request->price,
                'photo' => $photo,
            ]);
            return response()->json(['message' => "تم حفظ الكورس بنجاح"]);
        } catch (\Exception $e) {
            return response()->json(['message' => "خطأ اثناء حفظ الكورس"]);
        }
    }

    public function find($id)
    {
        $course = Course::findOrFail($id);
        return response()->json($course);
    }

    public function update(Request $request, $id)
    {
        try {
            $request->validate([
                'name' => 'required',
            ]);
            $course = Course::findOrFail($id);
            $course->name = $request->name;
            $course->price = $request->price;
            if ($request->hasFile('photo')) {
                // $photo = $request->file('photo');
                // $name = time() . '.' . $photo->getClientOriginalExtension();
                // $destinationPath = public_path('/course');
                // $photo->move($destinationPath, $name);
                // $course->photo = $name;
                if($course->photo != null){
                    \Storage::delete($course->photo);
                }
                $photo = $request->photo->store('/public/course');
                $course->photo = $photo;
            }
            $course->update();
            return response()->json(['message' => "تم تعديل الكورس بنجاح"]);
        } catch (\Exception $e) {
            return response()->json(['message' => "خطأ اثناء تعديل الكورس"]);
        }
    }

    public function delete($id)
    {
        try {
            $course = Course::findOrFail($id);
            if($course->photo != null){
                \Storage::delete($course->photo);
            }
            $course->courseVideos()->delete();
            $course->courseBooks()->delete();
            $course->subscriberCourses()->delete();
            $course->delete();
            return response()->json(['message' => "تم حذف الكورس بنجاح"]);
        } catch (\Exception $e) {
            return response()->json(['message' => "خطأ اثناء حذف الكورس"]);
        }

    }
}
