<?php

namespace App\Http\Controllers;

use App\Models\Book;
use Illuminate\Http\Request;

class BookController extends Controller
{
    //
    public function show()
    {
        $books = Book::get();
        return response()->json($books);
    }

    public function store(Request $request)
    {
        try {
            if ($request->name == '' || $request->file == '') {
                return response()->json(['message' => "من فضلك اكمل البيانات"]);
            }
            $file = $request->file->store('/public/book');

            $book = Book::create([
                'name' => $request->name,
                'author'=> $request->author,
                'description'=> $request->description,
                'file'=> $file,

            ]);
            return response()->json(['message' => "تم حفظ الكتاب بنجاح"]);
        } catch (\Exception $e) {
            return response()->json(['message' => "خطأ اثناء حفظ الكتاب"]);
        }
    }

    public function find($id)
    {
        $book = Book::findOrFail($id);
        return response()->json($book);
    }

    public function update(Request $request, $id)
    {
        try {
            $request->validate([
                'name' => 'required',
            ]);

            $book = Book::findOrFail($id);
            $book->name = $request->name;
            $book->author = $request->author;
            $book->description = $request->description;
            if ($request->hasFile('file')) {
                if($book->file != null){
                    \Storage::delete($book->file);
                }
                $file = $request->file->store('/public/book');
                $book->file = $file;
            }
            $book->update();
            return response()->json(['message' => "تم تعديل الكتاب بنجاح"]);
        } catch (\Exception $e) {
            return response()->json(['message' => "خطأ اثناء تعديل الكتاب"]);
        }
    }

    public function delete($id)
    {
        try {
            $book = Book::findOrFail($id);
            if($book->file != null){
                \Storage::delete($book->file);
            }
            $book->courseBooks()->delete();
            $book->delete();
            return response()->json(['message' => "تم حذف الكتاب بنجاح"]);
        } catch (\Exception $e) {
            return response()->json(['message' => "خطأ اثناء حذف الكتاب"]);
        }

    }
}
