<?php

namespace App\Http\Controllers;

use App\Models\CourseVideo;
use Illuminate\Http\Request;

class CourseVideoController extends Controller
{
    public function show()
    {
        $courseVideos = CourseVideo::get();
        return response()->json($courseVideos);
    }

    public function store(Request $request)
    {
        try {
            if ($request->course_id == '' || $request->name == '' || $request->video == '') {
                return response()->json(['message' => "من فضلك اكمل البيانات"]);
            }

            $video = $request->video->store('/public/course/video');

            $courseVideo = CourseVideo::create([
                'course_id'=> $request->course_id,
                'name' => $request->name,
                'description'=> $request->description,
                'video' => $video,

            ]);
            return response()->json(['message' => "تم حفظ الفيديو فى الكورس بنجاح"]);
        } catch (\Exception $e) {
            return response()->json(['message' => "خطأ اثناء حفظ الفيديو فى الكورس"]);
        }
    }

    public function find($id)
    {
        $courseVideo = CourseVideo::findOrFail($id);
        return response()->json($courseVideo);
    }

    public function update(Request $request, $id)
    {
        try {
            $request->validate([
                'name' => 'required',
            ]);
            $courseVideo = CourseVideo::findOrFail($id);
            $courseVideo->course_id = $request->course_id;
            $courseVideo->name = $request->name;
            $courseVideo->description = $request->description;

            if ($request->hasFile('video')) {
                if($courseVideo->video != null){
                    \Storage::delete($courseVideo->video);
                }
                $video = $request->video->store('/public/course/video');
                $courseVideo->video = $video;
            }
            $courseVideo->update();
            return response()->json(['message' => "تم تعديل الفيديو بنجاح"]);
        } catch (\Exception $e) {
            return response()->json(['message' => "خطأ اثناء تعديل الفيديو"]);
        }
    }

    public function delete($id)
    {
        try {
            $courseVideo = CourseVideo::findOrFail($id);
            if($courseVideo->video != null){
                \Storage::delete($courseVideo->video);
            }
            $courseVideo->delete();
            return response()->json(['message' => "تم حذف الفيديو بنجاح"]);
        } catch (\Exception $e) {
            return response()->json(['message' => "خطأ اثناء حذف الفيديو"]);
        }
    }
}
