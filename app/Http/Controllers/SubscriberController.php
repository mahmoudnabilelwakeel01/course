<?php

namespace App\Http\Controllers;

use App\Models\Subscriber;
use Illuminate\Http\Request;

class SubscriberController extends Controller
{
    public function show()
    {
        $subscribers = Subscriber::get();
        return response()->json($subscribers);
    }

    public function store(Request $request)
    {
        try {
            if ($request->name == '' ) {
                return response()->json(['message' => "من فضلك اكمل البيانات"]);
            }
            $subscriber = Subscriber::create([
                'name' => $request->name,
                'mobile'=> $request->mobile,
                'address'=> $request->address,
            ]);
            return response()->json(['message' => "تم حفظ المشترك بنجاح"]);
        } catch (\Exception $e) {
            return response()->json(['message' => "خطأ اثناء حفظ المشترك"]);
        }
    }

    public function find($id)
    {
        $subscriber = Subscriber::findOrFail($id);
        return response()->json($subscriber);
    }

    public function update(Request $request, $id)
    {
        try {
            $request->validate([
                'name' => 'required',
            ]);

            $subscriber = Subscriber::findOrFail($id);
            $subscriber->name = $request->name;
            $subscriber->mobile = $request->mobile;
            $subscriber->address = $request->address;
            $subscriber->update();
            return response()->json(['message' => "تم تعديل المشترك بنجاح"]);
        } catch (\Exception $e) {
            return response()->json(['message' => "خطأ اثناء تعديل المشترك"]);
        }
    }

    public function delete($id)
    {
        try {
            $subscriber = Subscriber::findOrFail($id);
            $subscriber->subscriberCourses()->delete();
            $subscriber->delete();
            return response()->json(['message' => "تم حذف المشترك بنجاح"]);
        } catch (\Exception $e) {
            return response()->json(['message' => "خطأ اثناء حذف المشترك"]);
        }

    }
}
