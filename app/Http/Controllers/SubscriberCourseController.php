<?php

namespace App\Http\Controllers;

use App\Models\SubscribeCourse;
use Illuminate\Http\Request;

class SubscriberCourseController extends Controller
{
    public function show()
    {
        $subscribeCourses = SubscribeCourse::get();
        return response()->json($subscribeCourses);
    }

    public function store(Request $request)
    {
        try {
            if ($request->course_id == '' || $request->subscriber_id == '' ) {
                return response()->json(['message' => "من فضلك اكمل البيانات"]);
            }

            $subscribeCourse = SubscribeCourse::create([
                'course_id'=> $request->course_id,
                'subscriber_id'=> $request->subscriber_id,
            ]);
            return response()->json(['message' => "تم إضافة كورس لمشترك بنجاح"]);
        } catch (\Exception $e) {
            return response()->json(['message' => "خطأ اثناء إضافة الكورس للمشترك"]);
        }
    }

    public function find($id)
    {
        $subscribeCourse = SubscribeCourse::findOrFail($id);
        return response()->json($subscribeCourse);
    }

    public function update(Request $request, $id)
    {
        try {
            $request->validate([
                'course_id' => 'required',
                'subscriber_id' => 'required',
            ]);
            $subscribeCourse = SubscribeCourse::findOrFail($id);
            $subscribeCourse->course_id = $request->course_id;
            $subscribeCourse->subscriber_id = $request->subscriber_id;

            $subscribeCourse->update();
            return response()->json(['message' => "تم تعديل المشترك بنجاح"]);
        } catch (\Exception $e) {
            return response()->json(['message' => "خطأ اثناء تعديل المشترك"]);
        }
    }

    public function delete($id)
    {
        try {
            $subscribeCourse = SubscribeCourse::findOrFail($id);
            $subscribeCourse->delete();
            return response()->json(['message' => "تم حذف الكورس من المشترك بنجاح"]);
        } catch (\Exception $e) {
            return response()->json(['message' => "خطأ اثناء حذف الكورس من المشترك"]);
        }
    }
}
