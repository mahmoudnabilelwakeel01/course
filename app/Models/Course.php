<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'price',
        'photo',
    ];

    /**
     * Get all of the comments for the Course
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function courseVideos()
    {
        return $this->hasMany(CourseVideo::class, 'course_id', 'id');
    }
    public function courseBooks()
    {
        return $this->hasMany(CourseBook::class, 'course_id', 'id');
    }
    public function subscriberCourses()
    {
        return $this->hasMany(SubscribeCourse::class, 'course_id', 'id');
    }
}
