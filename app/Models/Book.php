<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'author',
        'description',
        'file',
    ];

    public function courseBooks()
    {
        return $this->hasMany(CourseBook::class, 'book_id', 'id');
    }
}
